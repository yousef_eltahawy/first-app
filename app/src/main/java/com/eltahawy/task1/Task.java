package com.eltahawy.task1;

public class Task {
    private String Task;
    private String Description;
    public Task(String task,String description){
        this.Task=task;
        this.Description=description;
    }
    public void setTask(String task){
        this.Task=task;
    }
    public void setDescription(String description){
        this.Description=description;
    }
    public String getTask() {
        return this.Task;
    }
    public String getDescription() {
        return this.Description;
    }
}
