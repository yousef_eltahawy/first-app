package com.eltahawy.task1;

import java.io.File;
import java.text.Format;
import java.util.ArrayList;

public interface TaskDao {
    void addTask(Task task,File file)throws Exception;
    void editTask(String taskName,Task task,File file);
    void deletTask(String task,File file);
    ArrayList<Task> getAllTasks(File file);
}