package com.eltahawy.task1;

import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import android.support.design.widget.FloatingActionButton;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;


public  class MyFile {

    public static void putInFile(ArrayList<Task> list,File file) throws Exception {
        clearFile(file);
        int index=0;
        FileOutputStream fout = new FileOutputStream(file,true);
        while (!list.isEmpty()) {
            Task task = list.get(index);
            fout.write((task.getTask() + "/" + task.getDescription() + "|").getBytes());
            index++;
        }
        fout.close();
    }

    public static ArrayList<Task> getFromFile(File file) throws Exception {
        ArrayList<Task> arr =new ArrayList<Task>();
        FileInputStream fin = new FileInputStream(file);
        int Read;
        String task="" ,description="" ;
        String temp = "";
        while (((Read = fin.read())) != -1) {
            if (Read != 47 && Read != 124){
                temp = temp + ((char) Read);
            }
            else if(Read == 47) {
                task = temp;
                temp = "";
            }
            else if(Read == 124){
                description=temp;
                temp="";
                arr.add(new Task(task,description));
            }
        }
        fin.close();
        return arr;
    }
    public static void clearFile(File file) throws Exception{

            FileOutputStream fout =new FileOutputStream(file);
            fout.write(("").getBytes());
            fout.close();
    }
}