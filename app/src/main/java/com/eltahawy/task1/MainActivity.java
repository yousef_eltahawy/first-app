package com.eltahawy.task1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    EditText editText ;
    Button button;
    TextView textView;
    String newFileName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText=findViewById(R.id.name);
        button=findViewById(R.id.Next_button);
        textView=findViewById(R.id.NewList);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new General().validate(editText))
                    if (getFileStreamPath((editText.getText().toString() + ".txt")).exists()) {
                        Intent intent = new Intent(MainActivity.this, Activity_tasks.class);
                        intent.putExtra("text", editText.getText().toString());
                        startActivity(intent);
                        editText.getText().clear();
                    }
            }
        });
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(new General().validate(editText)) {
                    newFileName = editText.getText().toString();
                    File file = new File("/data/data/com.eltahawy.task1/files/" + newFileName + ".txt");
                    try {
                        if (!file.exists())
                            file.createNewFile();
                    } catch (Exception e) {
                    }
                    Toast.makeText(getApplicationContext(),"the "+newFileName+" file has been created",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
