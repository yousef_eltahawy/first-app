package com.eltahawy.task1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class TaskDaoImplemention  implements TaskDao {
    @Override
    public void addTask(Task task, File file) {
        try {
            ArrayList<Task> list = MyFile.getFromFile(file);
            list.add(task);
            MyFile.putInFile(list, file);
        } catch (Exception e) {
        }
    }
    @Override
    public ArrayList<Task> getAllTasks(File file) {
        ArrayList<Task> list = new ArrayList<Task>();
        try {
            list = MyFile.getFromFile(file);
        } catch (Exception e) {
        }
        return list;
    }

    @Override
    public void editTask(String taskName ,Task task, File file) {
        try {
            ArrayList<Task> list = MyFile.getFromFile(file);
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getTask().equals(taskName)){
                    list.remove(i);
                    list.add(i,task);
                }
            }
            MyFile.putInFile(list, file);
        } catch (Exception e) {
        }
    }

    @Override
    public void deletTask(String task, File file) {
        try {
            ArrayList<Task> list = MyFile.getFromFile(file);

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getTask().equals(task))
                    list.remove(i);
            }
            MyFile.putInFile(list, file);
        } catch (Exception e) {
        }

    }

}
