package com.eltahawy.task1;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;


public class General{
    public boolean validate(EditText ET) {
        String Str = ET.getText().toString().trim();

        if (Str.isEmpty()) {
            ET.setError("Field can't be empty");
            return false;
        } else {
            ET.setError(null);
            return true;
        }

    }

}
