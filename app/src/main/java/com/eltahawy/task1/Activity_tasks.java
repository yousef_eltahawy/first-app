package com.eltahawy.task1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import android.support.design.widget.FloatingActionButton;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.File;
import java.util.Iterator;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Activity_tasks extends AppCompatActivity implements Dialog.DialogListener {
    public static int flag=0;
    public static int index;
    public static String task,description;
    TextView textView;
    ArrayList<String> tasks;
    ListView listView;
    ArrayAdapter<String> Adapter;
    FloatingActionButton floatingActionButton;
    File file;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        textView = findViewById(R.id.text);
        file=getFileStreamPath(getIntent().getStringExtra("text")+".txt");
        System.out.println(file);
        tasks =UpdateTaskList(new TaskDaoImplemention().getAllTasks(file)) ;
        floatingActionButton = findViewById(R.id.fab);
        textView.setText("Hello " + getIntent().getStringExtra("text") + " !");
        Adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,tasks);
        listView = findViewById(R.id.List);
        listView.setAdapter(Adapter);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                PopupMenu popup = new PopupMenu(Activity_tasks.this, view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.Edit_item:{
                                flag=1;
                                task= UpdateTaskList(new TaskDaoImplemention().getAllTasks(file)).get(position);
                                description=UpdateDescriptionList(new TaskDaoImplemention().getAllTasks(file)).get(position);
                                openDialog();
                                listView.invalidateViews();
                                return true;}
                            case R.id.Delete_item:{
                                new TaskDaoImplemention().deletTask(tasks.get(position),file);
                                tasks.remove(position);
                                listView.invalidateViews();
                                return true;}
                            default:
                                return true;
                        }
                    }
                });

                popup.show();
                return true;
            }
        });
    }
    // to update the list
    public ArrayList<String> UpdateTaskList( ArrayList<Task> list) {
        ArrayList<String> list1 =new ArrayList<String>();
         Iterator itr=list.iterator();
            while(itr.hasNext())
            {
                Task obj = (Task) itr.next();
                list1.add(0,obj.getTask());
            }
            return list1;
    }
    public ArrayList<String> UpdateDescriptionList( ArrayList<Task> list) {
        ArrayList<String> list1 =new ArrayList<String>();
        Iterator itr=list.iterator();
        while(itr.hasNext())
        {
            Task obj = (Task) itr.next();
            list1.add(0,obj.getDescription());
        }
        return list1;
    }

    public  void  openDialog(){
        Dialog dialog=new Dialog();
        dialog.show(getSupportFragmentManager(),"Task Dialog");
    }


    // override from DialogListerner interface
    @Override
    public void setTask(Task task) {

        tasks.add(0,task.getTask());
        new TaskDaoImplemention().addTask(task,file);
    }
    @Override
    public void editTask(Task task,int index){
        new TaskDaoImplemention().editTask(this.task,task,file);
        tasks.remove(index);
        tasks.add(index,task.getTask());
    }
}