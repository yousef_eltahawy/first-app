package com.eltahawy.task1;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Dialog extends AppCompatDialogFragment {
    private EditText taskName;
    private EditText Description;
    private DialogListener listener;
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.layout_dialog,null);
        taskName=view.findViewById(R.id.task_name);
        Description=view.findViewById(R.id.description);
        if(Activity_tasks.flag==1){
            Activity_tasks.flag=0;
            taskName.setText(Activity_tasks.task);
            Description.setText(Activity_tasks.description);
            builder.setView(view)
                    .setTitle("Edit Task")
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(new General().validate(taskName))
                                if (new General().validate(Description)){
                                    Task task=new Task(taskName.getText().toString(),Description.getText().toString());
                                    listener.editTask(task,Activity_tasks.index);
                                    Toast.makeText(getContext(),
                                            "Done!",
                                            Toast.LENGTH_LONG)
                                            .show();
                                }

                        }
                    });
            return builder.create();
        }
        else{
            builder.setView(view)
                    .setTitle("Add Task")
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(new General().validate(taskName))
                                if (new General().validate(Description)){
                                    listener.setTask(new Task(taskName.getText().toString(),Description.getText().toString()));
                                    Toast.makeText(getContext(),
                                            "Done!",
                                            Toast.LENGTH_LONG)
                                            .show();
                                }

                        }
                    });
            return builder.create();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener=(DialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()+"must implement DialogListener");
        }
    }


    public interface DialogListener{
        void setTask(Task task);
        void editTask(Task task,int index);
    }

}
